Homework 5
==============


1.	Workload:

	- Dinh Phuong Nam: Design operations for all classes (signature, goal, parameter description, output description, design class relationship between classes (Generalization, Association, Aggregation and Composition, class diagram for ticket package and screen package, review.

	- Nguyen Ngoc Lam: Design attributes for all classesDesign attributes for all classes, Organize classes using packages, class diagram for controller package and interface package,package diagram for the whole software.


2.	Note:

	- **The report pictures are in the class-diagram-afc folder**

	- Class diagram is in version ~~2.0~~ 3.0 (**final**).

	- **class-diagram-afc-final.asta** *contains all* the communication diagrams but for grading purpose, we decided to seperate all communication diagrams plus the unify analysis diagram to the **communication-afc.asta**
	
	- Also make some change in the **SequenceDiagram** to match the new version of sequence diagram.

	- Add an pdf file for class design and data modeling discription.
