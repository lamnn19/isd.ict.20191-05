Homework 6
==============


1.	Workload:

	- Dinh Phuong Nam: Redesign package diagram, write classDesignandDataModeling.pdf, version one of database design.

	- Nguyen Ngoc Lam: ER diagram, change the database design for better workflow, review the pdf file.


2.	Note:

	- Scraped database design version 1.0

	- Database design is in version 2.0.

	- Database design in **database-design.asta** *is classified* as **ER diagram** at **Logical Data Model** as a **Relational Data Model**.
	
	- er-diagram.png is the **Conceptual Data Model**

	- classDesignandDataModeling.pdf covered class design and data modeling discription.
