Homework 3
==============


1.	Workload:

	- Dinh Phuong Nam: sequence diagram for checkTicketValidation and chooseDepatureArrival; review

	- Nguyen Ngoc Lam: sequence diagram for 3 use cases regrading 3 types of boarding pass (24 hour, one way, prepaid)


2.	Note:

	- **The report pictures are in the sequence-diagram-afc folder**

	- Updated to *version 2.0*
