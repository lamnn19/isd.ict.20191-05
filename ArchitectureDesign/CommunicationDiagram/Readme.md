Homework 4
==============


1.	Workload:

	- Dinh Phuong Nam: communication diagram for 24HourTicketDisembrakation and 24HourTicketEmbrakation; merge all communication diagram to create AFCUnifyAnalysisDiagram; create the class diagram for the AFC.

	- Nguyen Ngoc Lam: communication diagram for checkTicketValidation, oneWayTicket, prepaidCardDisembrakation and prepaidCardEmbrakation, create class diagram per use case; review.


2.	Note:
	
	- Updated to version 2.0 (included per use case class diagrams)

	- **The report picture for class diagram is in the class-diagram-afc folder**

	- **The report pictures for communication diagrams and unify analysis diagram is in the communication-afc folder**

	- **The report pictures for each use case class diagram are in the class-diagram-per-use-case folder**

	- Included the folder sequence-diagram to make the work *easier to follow*. The report pictures are in sequence-diagram-afc.

	- Class diagram is in the *initial* phase.

	- **class-diagram-afc.asta** *contains all* the communication diagrams but for grading purpose, we decided to seperate all communication diagrams plus the unify analysis diagram to the **communication-afc.asta**
	
	- Also make some change in the **SequenceDiagram** to match the new version of sequence diagram.
