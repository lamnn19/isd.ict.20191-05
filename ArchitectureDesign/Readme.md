# Architecture Design

## Note:

1.	**Homework 3** *(about sequence diagram)* is updated to version 2.0.

2.	**Homework 4** *(about communication diagram and class diagram)* is in the **CommunicationDiagram** folder. 

3.	**Homework 5** *(about class design and packages)* is in the **ClassDesign** folder.

4.	Package diagram in **ClassDesign** folder is updated to version 2.0.

5.	Class diagram in **CommunicationDiagram** folder is updated to version 2.0

6.	**Homework 6** *(about data modeling)* is in the **Datamodeling** folder.

7.	Database design is in version 2.0.

8.	**Homework 7** *(about interface design)* is in the **InterfaceDesign** folder

9.	Added couplingCohesion.docx as **homework 9**

10.	Added addition-requirements-report.docx and modified ClassDesign/class-diagram-afc.asta (image exported as ClassDesign/class-diagram-afc/PackageDiagram_modified.png) as **homework 10**




