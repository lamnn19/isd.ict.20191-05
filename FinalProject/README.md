This is the repository for ISD.ICT.20191.
==============
Subject: ITSS Software Development
--------------

*Group: 5*

*Nguyen Ngoc Lam*

*Dinh Phuong Nam*

1. How to run:

	-	Get data: (see futher instruction at the readme file from data folder)

		- run data/afc-database.sql on psql

		- or if failed, run data/afc-database-if-failed.sql on psql, open pgadmin4 and manually import each csv file into the table that has the same name

	-	Open terminal or command line and open to the directory stored.

	-	Type "java -jar afc.jar" to run.

2. Assign work:
	
	- Ngyen Ngoc Lam: database design, controller class, main

	- Dinh Phuong Nam: Documentation, entities, UI design

3. Assessment:
	
	- Nguyen Ngoc Lam: late, has weird coding convention that need a lot of refactoring.
	
	- Dinh Phuong Nam: has some typos, follow coding convention

4. SRS: filename: SRS-UGMS-EN.docx

5. SDD: filename: SDD.docx

6. Coupling and Cohesion: filename couplingCohesion.docx

7. Note:
	
	- We do not use any design pattern
	
	- Because of our implementation, the open/close principle is compromised.