# Coding exercise

### Note: Open the xxx/doc/index.html for helping to use the SDK

## Week 8: first implementation

1.	Workload:

	- Dinh Phuong Nam: coding convention, CardScannerControllers and TicketRecognizerControllers implementation and test case.

	- Nguyen Ngoc Lam: implematation and test case on classes on afc.intities.


## Week 9: detail about coupling and cohesion

1.	Workload:

	- Dinh Phuong Nam: coupling for interface and controller package, cohesion ticket and screen package, first draft of the document.

	- Nguyen Ngoc Lam: coupling for ticket and screen package, cohesion interface and controller package, review document.
