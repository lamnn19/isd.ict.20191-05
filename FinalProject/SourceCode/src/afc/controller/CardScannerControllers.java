package afc.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import afc.entities.Card;
import afc.entities.OneWayTicket;
import afc.entities.PrepaidCard;

/**
 * Controller class of card scanner. Included: change from pseudo bar code
 * to card id and get Card base on type and id
 * 
 * @version 2.0
 * @author nam
 *
 */
public class CardScannerControllers {
	/**
	 * Change from pseudo bar code to card id
	 * 
	 * @param barcode bar code that the user has
	 * @return card id (started with PC); null otherwise (included
	 *         invalid bar code or bar code of a ticket)
	 */
	public String getCardId(String barcode) {
		DatabaseConnector ct = new DatabaseConnector();
		Connection conn = ct.connect();
		String param = "'" + barcode + "'";
		String result = null;

		String SQL = "select id from afcdata.mapping as m where m.barcode = " + param;
		try (Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(SQL)) {
			while (rs.next()) {
				result = rs.getString("id");
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		String pattern = "^PC";
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(result);
		if (!m.find())
			result = null;
		ct.close(conn);
		return result;
	}
	
	/**
	 * Get instance of prepaid card
	 * 
	 * @param id id of the instance
	 * @return an {@link PrepaidCard} object if id started with PC; null otherwise
	 */
	public PrepaidCard getPrepaidCard(String id) {
		String pattern = "^PC";
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(id);
		DatabaseConnector ct = new DatabaseConnector();
		Connection conn = ct.connect();
		if (m.find()) {
			String param = "'" + id + "'";
			String SQL = "select * from afcdata.prepaidcard where id = " + param;
			try (Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(SQL)) {
				while (rs.next()) {
					String nid = rs.getString("id");
					boolean nvalid = rs.getBoolean("valid");
					double nbalance = rs.getDouble("balance");
					PrepaidCard result = new PrepaidCard(nid, nvalid, nbalance);
					ct.close(conn);
					return result;
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
		}
		ct.close(conn);
		return null;
	}
}
