package afc.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import afc.entities.DayTicket;
import afc.entities.OneWayTicket;
import afc.entities.PrepaidCard;
import afc.entities.Station;
import afc.entities.Transaction;

/**
 * Controller class of AFC. Included: get list of station, decide if the
 * ticket/card is valid to go or not and restart the whole AFC
 * 
 * @version 2.0
 * @author lam
 */
public class AFCControllers {
	protected OneWayTicket oneway = null;
	protected DayTicket day = null;
	protected PrepaidCard prepaid = null;
	protected Transaction transac = null;

	/**
	 * Get the list of station in the line. Cannot use for multiple line; if want to
	 * use it on multiple line, change structure of database first, and subsequently
	 * change String SQL variable
	 * 
	 * @return List of Station as one line.
	 */
	public List<Station> listofStation() {
		DatabaseConnector ct = new DatabaseConnector();
		Connection conn = ct.connect();
		List<Station> line = new ArrayList<Station>();
		String SQL = "select * from afcdata.station";
		try (Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(SQL)) {
			while (rs.next()) {
				String name = rs.getString("name");
				String next = rs.getString("nextstation");
				double dn = rs.getDouble("distancetonext");
				String previous = rs.getString("previousstation");
				double dp = rs.getDouble("distancetoprevious");
				Station toappend = new Station(name, next, previous, dn, dp);
				line.add(toappend);
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		ct.close(conn);
		return line;
	}

	/**
	 * Get a station.
	 * 
	 * @param name Name of that station
	 * @return one Station with name.
	 */
	public Station getStation(String name) {
		DatabaseConnector ct = new DatabaseConnector();
		Connection conn = ct.connect();
		String param = "'" + name + "'";

		String SQL = "select * from afcdata.station where name = " + param;
		try (Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(SQL)) {
			while (rs.next()) {
				String nname = rs.getString("name");
				String next = rs.getString("nextstation");
				double dn = rs.getDouble("distancetonext");
				String previous = rs.getString("previousstation");
				double dp = rs.getDouble("distancetoprevious");
				Station toappend = new Station(nname, next, previous, dn, dp);
				return toappend;
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		ct.close(conn);
		return null;
	}

	/**
	 * Decide if the ticket/card is valid to go or not and push new information to
	 * database
	 * 
	 * @param start name of the start station
	 * @param stop  name of the stop station
	 * @param line  list of station (in one line)
	 * @param id    id of the ticket/card in database
	 * @return true if the ticket/card can open the gate; false otherwise
	 */
	@SuppressWarnings("unused")
	public boolean isValid(String start, String stop, List<Station> line, String id) {
		Long aday = 1000L * 3600 * 24;
		Long clear = Instant.now().toEpochMilli();
		DatabaseConnector ct = new DatabaseConnector();
		Connection conn = ct.connect();
		TicketRecognizerControllers trc = new TicketRecognizerControllers();
		CardScannerControllers csc = new CardScannerControllers();
		FareCalculator calculator = new ByDistance(start, stop);
		double fare = calculator.calculateFare(line);
		Station startstation = getStation(start);
		Station stopstation = getStation(stop);
		this.oneway = trc.getOneWayTicket(id);
		if (this.oneway == null) {
			this.prepaid = csc.getPrepaidCard(id);
			if (this.prepaid != null) {
				if (this.prepaid.getBalance() < fare) {
					String SQL = "update afcdata.prepaidcard set valid = ? where id = ?";
					try (PreparedStatement pstmt = conn.prepareStatement(SQL)) {

						pstmt.setBoolean(1, false);
						pstmt.setString(2, this.prepaid.getId());

						int affectedrows = pstmt.executeUpdate();

					} catch (SQLException ex) {
						System.out.println(ex.getMessage());
					}
					ct.close(conn);
					return false;
				} else {
					String SQL = "update afcdata.prepaidcard set balance = ? where id = ?";
					try (PreparedStatement pstmt = conn.prepareStatement(SQL)) {

						pstmt.setDouble(1, this.prepaid.getBalance() - fare);
						pstmt.setString(2, this.prepaid.getId());

						int affectedrows = pstmt.executeUpdate();

					} catch (SQLException ex) {
						System.out.println(ex.getMessage());
					}
					ct.close(conn);
					this.transac = new Transaction(null, this.prepaid, fare, startstation, stopstation);
					return true;
				}
			} else {
				this.day = trc.getDayTicket(id);
				if (this.day.getBalance() < fare) {
					String SQL = "update afcdata.dayticket set valid = ? where id = ?";
					try (PreparedStatement pstmt = conn.prepareStatement(SQL)) {

						pstmt.setBoolean(1, false);
						pstmt.setString(2, this.day.getId());

						int affectedrows = pstmt.executeUpdate();

					} catch (SQLException ex) {
						System.out.println(ex.getMessage());
					}
					ct.close(conn);
					return false;
				} else if (this.day.getStartdate() + aday < clear) {
					String SQL = "update afcdata.dayticket set valid = ? where id = ?";
					try (PreparedStatement pstmt = conn.prepareStatement(SQL)) {

						pstmt.setBoolean(1, false);
						pstmt.setString(2, this.day.getId());

						int affectedrows = pstmt.executeUpdate();

					} catch (SQLException ex) {
						System.out.println(ex.getMessage());
					}
					ct.close(conn);
					return false;
				} else {
					String SQL = "update afcdata.dayticket set balance = ? where id = ?";
					try (PreparedStatement pstmt = conn.prepareStatement(SQL)) {

						pstmt.setDouble(1, this.day.getBalance() - fare);
						pstmt.setString(2, this.day.getId());

						int affectedrows = pstmt.executeUpdate();

					} catch (SQLException ex) {
						System.out.println(ex.getMessage());
					}
					ct.close(conn);
					this.transac = new Transaction(this.day, null, fare, startstation, stopstation);
					return true;
				}
			}
		} else {
			int i = 0;
			List<String> subline = new ArrayList<String>();
			while (!line.get(i).getName().equals(this.oneway.getStart().getName())) {
				if (line.get(i).getName().equals(this.oneway.getDestination().getName())) {
					while (line.get(i).getName().equals(this.oneway.getStart().getName())) {
						subline.add(line.get(i).getName());
						i++;
					}
					break;
				}
				i++;
			}
			for (int j = i; j < line.size(); j++)
				subline.add(line.get(j).getName());
			if ((!subline.contains(start)) || (!subline.contains(stop))) {
				String SQL = "update afcdata.onewayticket set valid = ? where id = ?";
				try (PreparedStatement pstmt = conn.prepareStatement(SQL)) {

					pstmt.setBoolean(1, false);
					pstmt.setString(2, this.oneway.getId());

					int affectedrows = pstmt.executeUpdate();

				} catch (SQLException ex) {
					System.out.println(ex.getMessage());
				}
				ct.close(conn);
				return false;
			} else {
				ct.close(conn);
				this.transac = new Transaction(this.day, null, fare, startstation, stopstation);
				return true;
			}
		}
	}

	/**
	 * Get the ticket/card information an print it on the screen
	 */
	public void getInformation() {
		if (this.oneway != null)
			System.out.println(this.oneway.toString());
		else if (this.day != null)
			System.out.println(this.day.toString());
		else
			System.out.println(this.prepaid.toString());
	}

	/**
	 * push the transaction value to database
	 */
	@SuppressWarnings("unused")
	public boolean pushTransaction() {
		if (this.transac != null) {
			DatabaseConnector ct = new DatabaseConnector();
			Connection conn = ct.connect();
			String SQL = "insert into afcdata.transaction values(?,?,?,?,?)";
			PreparedStatement pstmt = null;
			try {
				pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
			try {
				String id = null;
				if (this.transac.getTicket() == null)
					id = transac.getCard().getId();
				else
					id = transac.getTicket().getId();

				pstmt.setString(1, id);
				pstmt.setLong(2, transac.getTimestamp());
				pstmt.setString(3, transac.getStart().getName());
				pstmt.setString(4, transac.getStop().getName());
				pstmt.setDouble(5, transac.getFare());

				boolean affectedRows = pstmt.execute();
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
			ct.close(conn, pstmt);
			return true;
		} else
			return false;
	}

	/**
	 * Get the list of past transactions done by this barcode
	 * 
	 * @param barcode barcode of the user card
	 * @return transactions as ArrayList<Transaction> if found id, return null
	 *         otherwise
	 * @see ArrayList
	 * @see Transaction
	 */
	public List<Transaction> getPastTransaction(String barcode) {
		DatabaseConnector ct = new DatabaseConnector();
		Connection conn = ct.connect();
		List<Transaction> pasttransac = new ArrayList<Transaction>();
		String SQL = "select * from afcdata.transaction where id = ?";
		TicketRecognizerControllers trc = new TicketRecognizerControllers();
		CardScannerControllers csc = new CardScannerControllers();
		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		String userid = trc.getTicketId(barcode);
		if (userid == null)
			userid = csc.getCardId(barcode);
		if (userid == null)
			// not found
			System.out.println("Nothing!");
		else {
			DayTicket tf = trc.getDayTicket(userid);
			PrepaidCard pc = csc.getPrepaidCard(userid);
			OneWayTicket ow = trc.getOneWayTicket(userid);
			try {
				pstmt.setString(1, userid);
				ResultSet rs = pstmt.executeQuery();
				while (rs.next()) {
					long timestamp = (long) rs.getInt("timestamp");
					String startname = rs.getString("start");
					String stopname = rs.getString("stop");
					double fare = rs.getDouble("fare");

					Station start = getStation(startname);
					Station stop = getStation(stopname);

					if (tf != null) {
						Transaction toappend = new Transaction(tf, null, fare, start, stop);
						System.out.println(toappend.toString());
						pasttransac.add(toappend);
					} else if (ow != null) {
						Transaction toappend = new Transaction(ow, null, fare, start, stop);
						System.out.println(toappend.toString());
						pasttransac.add(toappend);
					} else {
						Transaction toappend = new Transaction(null, pc, fare, start, stop);
						System.out.println(toappend.toString());
						pasttransac.add(toappend);
					}
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
		}
		for (int i = 0; i < pasttransac.size(); i ++)
			System.out.println(pasttransac.get(i).toString());
		return pasttransac;
	}

	/**
	 * Function to restart the state of AFC controller. Use at the end of each loop
	 * on main.
	 * 
	 */
	public void restart() {
		this.oneway = null;
		this.day = null;
		this.prepaid = null;
		this.transac = null;
	}
}
