/**
 * Package contains representation of database data. In other word, each 
 * entities of of each class can be seen as a single row in one of the 
 * table in database
 */
package afc.entities;

/**
 * Actual implement of Prepaid card
 * 
 * @version 1.1
 * @author lam
 *
 */
public class PrepaidCard extends Card {

	/**
	 * Constructor of {@link PrepaidCard}
	 * 
	 * @param id      id of the card
	 * @param valid   last status of the ticket on database
	 * @param balance last balance on the card on database
	 * @see Card
	 */
	public PrepaidCard(String id, boolean valid, double balance) {
		super(id, valid, balance);
	}

	/**
	 * Used to compare two instances. At first version, use extensively. But after
	 * realizing if implement that way, system won't scale, change to one instance
	 * of ticket/card per 1 run (to push to database only).
	 * 
	 * @deprecated
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	/**
	 * Write out
	 */
	@Override
	public String toString() {
		if (super.valid) {
			return "PrepaidCard [id=" + this.getId() + ", balance=" + this.getBalance() + ", status:Approved" + "]";
		} else {
			return "PrepaidCard [id=" + this.getId() + ", balance=" + this.getBalance() + ", status:Denied" + "]";
		}
	}
}
