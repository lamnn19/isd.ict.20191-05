/**
 * Package contains representation of database data. In other word, each 
 * entities of of each class can be seen as a single row in one of the 
 * table in database
 */
package afc.entities;

/**
 * Super class of all ticket used in the system. Actual implement one-way and
 * 24-hour
 * 
 * @version 1.1
 * @author nam
 *
 */
public class Ticket {
	private String id;
	public boolean valid;

	/**
	 * Constructor of {@link Ticket}
	 * 
	 * @param id    id of the ticket
	 * @param valid last status of the ticket on database
	 */
	public Ticket(String id, boolean valid) {
		super();
		this.id = id;
		this.valid = valid;
	}

	/**
	 * Can the ticket open the gate or not
	 * 
	 * @return true if can open; false otherwise
	 */
	public boolean isValid() {
		return valid;
	}

	/**
	 * Get the id of the ticket
	 * 
	 * @return id of the ticket as String
	 */
	public String getId() {
		return id;
	}

	/**
	 * Hash function of ticket. Used to check. No longer needed.
	 * 
	 * @deprecated
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (valid ? 1231 : 1237);
		return result;
	}

	/**
	 * Used to compare two instances. At first version, use extensively. But after
	 * realizing if implement that way, system won't scale, change to one instance
	 * of ticket/card per 1 run (to push to database only).
	 * 
	 * @deprecated
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ticket other = (Ticket) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (valid != other.valid)
			return false;
		return true;
	}

	/**
	 * Write out
	 */
	@Override
	public String toString() {
		return "Ticket [id=" + id + ", valid=" + valid + "]";
	}
}
