package afc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;

import afc.controller.GateControllers;
import afc.controller.AFCControllers;
import afc.entities.Station;
import afc.entities.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import afc.controller.AFCControllers;
import afc.controller.ByDistance;
import afc.controller.CardScannerControllers;
import afc.controller.FareCalculator;
import afc.controller.TicketRecognizerControllers;
import afc.entities.Station;

public class UI extends Application {
	private Label label;
	AFCControllers afccontroller = new AFCControllers();
	List<Station> line = afccontroller.listofStation();
	CardScannerControllers cardscanner = new CardScannerControllers();
	TicketRecognizerControllers ticketrecognizers = new TicketRecognizerControllers();
	ArrayList<String> info = new ArrayList<String>();

	private void showAlertStation() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Warning alert");

		// Header Text: null
		alert.setHeaderText(null);
		alert.setContentText("Wrong station index! Must be in range 1-14");

		alert.showAndWait();
	}

	private void showAlertNonExistedTicket() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Warning alert");

		// Header Text: null
		alert.setHeaderText(null);
		alert.setContentText("Not existed ticket/card");

		alert.showAndWait();
	}

	private void showAlertInvalidTicket() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Warning alert");

		// Header Text: null
		alert.setHeaderText(null);
		alert.setContentText("Invalid/Used ticket");

		alert.showAndWait();
	}

	private void showAlertUnbalancedTicket() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Warning alert");

		// Header Text: null
		alert.setHeaderText(null);
		alert.setContentText("Not enough money!");

		alert.showAndWait();
	}

	private void showAlertWrongData() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Warning alert");

		// Header Text: null
		alert.setHeaderText(null);
		alert.setContentText("Wrong data! Enter again...");

		alert.showAndWait();
	}

	private String getStations(int inputstart, int inputstop) {
		if ((inputstart >= 1 && inputstart <= 14) && (inputstop >= 1 && inputstop <= 14)) {
			System.out.println(inputstart + "" + inputstop);

			String startStation = (line.get(inputstart - 1).getName());
			String stopStation = line.get(inputstop - 1).getName();
			info.add(startStation);
			info.add(stopStation);
			return startStation + " " + stopStation;
		} else
			return null;
	}

	private boolean checkValid() {
		System.out.println("Result");
		System.out.println(info.get(1));
		System.out.println(info.get(2));
		System.out.println(info.get(0));
//	    	for (int i = 0; i < info.size(); i++) {
//	    	      System.out.println(info.get(i));
//	    	    }
		String start = info.get(1).toString();
		String stop = info.get(2).toString();
		String id = info.get(0).toString();
		if (start != null && stop != null && id != null) {
			if (afccontroller.isValid(start, stop, line, id)) {
				System.out.println("Open gate.");
				return true;
			} else {
				return false;
			}
		} else
			return false;
	}

	private void displayData() {
		Label thirdlabel = new Label();
		VBox thirdroot = new VBox();
		thirdroot.setPadding(new Insets(20));
		thirdroot.setSpacing(15);
		Scene thirdScene = new Scene(thirdroot, 500, 350);
		Stage thirdWindow = new Stage();
		thirdWindow.setScene(thirdScene);
		thirdWindow.setTitle("Choose stations");
		thirdWindow.setScene(thirdScene);

		Text displayStation = new Text();
		displayStation
				.setText("Id: " + info.get(0) + "\n\nStart station: " + info.get(1) + "\nDestination: " + info.get(2));
		Button buttonReturn = new Button("Confirm");
		buttonReturn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				afccontroller.pushTransaction();
				afccontroller.restart();
				firstStage();
				thirdWindow.hide();
			}
		});
		thirdroot.getChildren().addAll(displayStation, buttonReturn, label);
		thirdWindow.show();
	}

	private void chooseStationWindow() {
		VBox root = new VBox();
		root.setPadding(new Insets(20));
		root.setSpacing(15);
		this.label = new Label();
		Scene secondScene = new Scene(root, 550, 550);
		Stage newWindow = new Stage();
		newWindow.setScene(secondScene);
		newWindow.setTitle("Choose stations");
		newWindow.setScene(secondScene);

		String stations = new String();
		Text printStation = new Text();
		for (int i = 0; i < line.size(); i++) {
			stations += (i + 1 + ". " + line.get(i).getName() + "\n");
		}
		printStation.setText(stations);

		Text enterStartStation = new Text();
		enterStartStation.setText("Enter start station:");
		TextField inputStartStation = new TextField();
		Text enterStopStation = new Text();
		enterStopStation.setText("Enter stop station:");
		TextField inputStopStation = new TextField();
		Text startStation = new Text();
		Text stopStation = new Text();
		Button buttonStation = new Button("Confirm");
		buttonStation.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (inputStartStation.getText().trim().isEmpty() && inputStopStation.getText().trim().isEmpty()) {
					showAlertStation();
				}
				int inputstart = Integer.parseInt(inputStartStation.getText());
				int inputstop = Integer.parseInt(inputStopStation.getText());

				String station = getStations(inputstart, inputstop);
				if (station != null) {
					if (checkValid()) {
						displayData();
						newWindow.hide();
					} else {
						showAlertInvalidTicket();
						displayData();
						newWindow.hide();
					}
				} else {
					showAlertStation();
				}
			}
		});
		root.getChildren().addAll(printStation, enterStartStation, inputStartStation, enterStopStation,
				inputStopStation, buttonStation, label);
		newWindow.show();

	}

	private String getTicket(String barcode) {
		String cardid1 = cardscanner.getCardId(barcode);
		String ticketid = ticketrecognizers.getTicketId(barcode);
		String thiscardid = null;
		String station = null;

		if (cardid1 != null) {
			thiscardid = cardid1;
			System.out.println("bar");
			info.add(thiscardid);
			return thiscardid;
		} else if (ticketid != null) {
			thiscardid = ticketid;
			System.out.println("code");
			info.add(thiscardid);
			return thiscardid;
		} else
			return null;
	}

	private void firstStage() {
		VBox firstroot = new VBox();
		// set position of stage
		firstroot.setPadding(new Insets(20));
		firstroot.setSpacing(15);
		Stage stage = new Stage();

		this.label = new Label();
		// print guild line
		Text enterBarcode = new Text();
		enterBarcode.setText("Enter your barcode:");
		TextField inputBarcode = new TextField();
		Button buttonBarcode = new Button("New Transaction");
		buttonBarcode.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				String barcode = inputBarcode.getText().toString();
				System.out.println(barcode);
				String thiscardid = getTicket(barcode);
				System.out.println(thiscardid);
				if (thiscardid != null) {
					chooseStationWindow();
					stage.hide();
				} else {
					showAlertNonExistedTicket();
				}
			}
		});
		Button buttonOld = new Button("View old Transaction");
		buttonOld.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				String barcode = inputBarcode.getText().toString();
				String thiscardid = getTicket(barcode);
				if (thiscardid != null) {
					showTransaction(barcode);
					stage.hide();
				} else {
					showAlertNonExistedTicket();
				}
			}
		});
		firstroot.getChildren().addAll(enterBarcode, inputBarcode, buttonBarcode, buttonOld, label);

		Scene scene = new Scene(firstroot, 350, 200);
		stage.setTitle("Automated Fare Collection");
		stage.setScene(scene);
		stage.show();
	}

	private void showTransaction(String barcode) {
		Label folabel = new Label();
		VBox foroot = new VBox();
		foroot.setPadding(new Insets(20));
		foroot.setSpacing(15);
		Scene foScene = new Scene(foroot, 500, 350);
		Stage foWindow = new Stage();
		foWindow.setScene(foScene);
		foWindow.setTitle("Transaction");
		foWindow.setScene(foScene);
		Text displayTransaction = new Text();

		List<Transaction> transaction = new ArrayList<Transaction>();
		transaction = afccontroller.getPastTransaction(barcode);
		if (transaction.size() == 0) {
			displayTransaction.setText("No old transaction");
		} else {
			// convert from transaction to string
			String history = "Due to security reason, we will not show the transaction here.\n But you have: " + transaction.size();
			displayTransaction.setText(history);
		}
		Button buttonReturn = new Button("Confirm");
		buttonReturn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				firstStage();
				foWindow.hide();
			}
		});
		foroot.getChildren().addAll(displayTransaction, buttonReturn, label);
		foWindow.show();
	}

	@Override
	public void start(Stage stage) {
		VBox root = new VBox();
		// set position of stage
		root.setPadding(new Insets(20));
		root.setSpacing(15);

		this.label = new Label();
		// print guild line
		Text enterWelcome = new Text();
		enterWelcome.setText("WELCOME TO \nAUTOMATED FARE COLLECTION");
		Button buttonMain = new Button("Start");
		buttonMain.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				firstStage();
				stage.hide();
			}
		});

		root.getChildren().addAll(enterWelcome, buttonMain, label);

		Scene scene = new Scene(root, 300, 150);
		stage.setTitle("Automated Fare Collection");
		stage.setScene(scene);
		stage.show();

	}

	public static void main(String args[]) {
		launch(args);
	}

}
