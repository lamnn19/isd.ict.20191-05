package afc.entities;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.Test;

class TransactionTest {
	Ticket testt = new DayTicket("a", true, 1235135135L, 45.6);
	Card testc = new PrepaidCard("b", true, 34.2);
	Station sta = new Station("a", "b", null, 100, 0);
	Station stb = new Station("b", null, "a", 0, 100);
	Transaction a = new Transaction(testt, testc, 12.5, sta, stb);
	Date date = new Date();
	Transaction b = new Transaction(testt, testc, 123456789L, 23.2, stb, sta);

	@Test
	void testConstructor() {
		long now = date.getTime();
		// assume that getTimestamp correct
		long testtime = a.getTimestamp();
		// assume that toString also correct
		String desired1 = "Transaction [ticket=" + testt + ", card=" + testc + ", " + "timestamp=" + testtime
				+ ", fare=" + 12.5 + "]";
		assertEquals(desired1, a.toString(), "Check implementation");
		// normally just a couple of millisecond behind
		assertTrue(now > testtime * 1000);
		String desired2 = "Transaction [ticket=" + testt + ", card=" + testc + ", " + "timestamp=" + 123456789L
				+ ", fare=" + 23.2 + "]";
		assertEquals(desired2, b.toString(), "Check implementation");
	}

	@Test
	void testGetTicket() {
		assertEquals(testt, a.getTicket(), "Unexpected error");
		assertEquals(testt, b.getTicket(), "Unexpected error");
		assertNotEquals(null, a.getTicket(), "Unexpected error");
		assertNotEquals(null, b.getTicket(), "Unexpected error");
	}

	@Test
	void testGetCard() {
		assertEquals(testc, a.getCard(), "Unexpected error");
		assertEquals(testc, b.getCard(), "Unexpected error");
		assertNotEquals(null, a.getCard(), "Unexpected error");
		assertNotEquals(null, b.getCard(), "Unexpected error");
	}

	@Test
	void testGetTimeStamp() {
		long now = date.getTime();
		long testtime = a.getTimestamp();
		// normally just a couple of millisecond behind
		assertTrue(now > testtime * 1000);
		assertEquals(123456789L, b.getTimestamp(), "Unexpected Error");
	}

	@Test
	void testGetFare() {
		assertEquals(12.5, a.getFare(), "Unexpected error");
		assertEquals(23.2, b.getFare(), "Unexpected error");
	}

	@Test
	void testGetStart() {
		assertEquals(sta, a.getStart(), "Unexpected error");
		assertEquals(stb, b.getStart(), "Unexpected error");
	}

	@Test
	void testGetStop() {
		assertEquals(stb, a.getStop(), "Unexpected error");
		assertEquals(sta, b.getStop(), "Unexpected error");
	}
}
