package afc.entities;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class OneWayTicketTest {
	Station testa = new Station("a", "", "b", 0, 100);
	Station testb = new Station("b", "c", "a", 100, 100);
	Station testc = new Station("c", "b", "", 100, 0);
	Station test = new Station("d", "", "", 0, 0);
	OneWayTicket ticket = new OneWayTicket("i", true, testa, testc);
	OneWayTicket ticket1 = new OneWayTicket("i1321", false, testa, testb);
	OneWayTicket ticket2 = new OneWayTicket("igh", true, testb, testc);
	
	@Test
	void testEqualsObject() {
		OneWayTicket newticket = new OneWayTicket("i", true, testa, testc);
		OneWayTicket falseticket1 = new OneWayTicket("o", true, testa, testc);
		OneWayTicket falseticket2 = new OneWayTicket("i", false, testa, testc);
		OneWayTicket falseticket3 = new OneWayTicket("i", true, testb, testc);
		OneWayTicket falseticket4 = new OneWayTicket("i", true, testa, testb);
		assertEquals(true, ticket.equals(newticket), "Unexpected false return");
		assertEquals(false, ticket.equals(falseticket1), "Do not implement on id field");
		assertEquals(false, ticket.equals(falseticket2), "Do not implement on valid field");
		assertEquals(false, ticket.equals(falseticket3), "Do not implement on start field");
		assertEquals(false, ticket.equals(falseticket4), "Do not implement on destination field");
	}
	
	@Test
	void testGetStart() {
		assertEquals(true, ticket.getStart().equals(testa), "Cannot get the correct start station. Expected station a. Got " + ticket.getStart().getName() + " instead.");
		assertEquals(true, ticket2.getStart().equals(testb), "Cannot get the correct start station. Expected station b. Got " + ticket2.getStart().getName() + " instead.");
	}

	@Test
	void testGetDestination() {
		assertEquals(true, ticket.getDestination().equals(testc), "Cannot get the correct destination station. Expected station c. Got " + ticket.getDestination().getName() + " instead.");
		assertEquals(true, ticket1.getDestination().equals(testb), "Cannot get the correct destination station. Expected station b. Got " + ticket2.getDestination().getName() + " instead.");
	}

	@Test
	void testGetId() {
		assertEquals("i", ticket.getId(), "Expected value of i. Get " + ticket.getId() + " instead.");
		assertEquals("i1321", ticket1.getId(), "Expected value of i1321. Get " + ticket1.getId() + " instead.");
		assertEquals("igh", ticket2.getId(), "Expected value of igh. Get " + ticket2.getId() + " instead.");
	}
	
	@Test
	void testToString() {
		String expected1 = "OneWayTicket [id=" + ticket.getId() + ", start=" + ticket.getStart() + ", destination=" + ticket.getDestination() + ", status:Approved" + "]";
		String expected2 = "OneWayTicket [id=" + ticket1.getId() + ", start=" + ticket1.getStart() + ", destination=" + ticket1.getDestination() + ", status:Denied" + "]";
		assertEquals(expected1, ticket.toString(), "Unexpected errors.");
		assertEquals(expected2, ticket1.toString(), "Unexpected errors.");
	}
}
