package afc.entities;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PrepaidCasrdTest {
	PrepaidCard card = new PrepaidCard("i", true, 0);
	PrepaidCard card2 = new PrepaidCard("i", false, 1000);
	
	@Test
	void testequals() {
		PrepaidCard newcard = new PrepaidCard("i", true, 0);
		PrepaidCard falsecard1 = new PrepaidCard("o", true, 0);
		PrepaidCard falsecard2 = new PrepaidCard("i", false, 0);
		PrepaidCard falsecard3 = new PrepaidCard("i", true, 10000);
		assertEquals(true, card.equals(newcard), "Unexpected false return");
		assertEquals(false, card.equals(falsecard1), "Do not implement on id field");
		assertEquals(false, card.equals(falsecard2), "Do not implement on valid field");
		assertEquals(false, card.equals(falsecard3), "Do not implement on balance field");
	}

	@Test
	void testGetBalance() {
		assertEquals(0, card.getBalance(), "Expected value of 0. Get " + card.getBalance() + " instead.");
		assertEquals(1000, card2.getBalance(), "Expected value of 1000. Get " + card2.getBalance() + " instead.");
	}

	@Test
	void testGetId() {
		PrepaidCard newcard = new PrepaidCard("isda4213", true, 0);
		assertEquals("i", card.getId(), "Expected value of i. Get " + card.getId() + " instead.");
		assertEquals("isda4213", newcard.getId(), "Expected value of isda4213. Get " + newcard.getId() + " instead.");
	}
	
	@Test
	void testToString() {
		String expected1 = "PrepaidCard [id=" + card.getId() + ", balance=" + card.getBalance() + ", status:Approved" + "]";
		String expected2 = "PrepaidCard [id=" + card2.getId() + ", balance=" + card2.getBalance() + ", status:Denied" + "]";
		assertEquals(expected1, card.toString(), "Unexpected errors.");
		assertEquals(expected2, card2.toString(), "Unexpected errors.");
	}
}
