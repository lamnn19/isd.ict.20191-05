package afc.controller;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import afc.entities.Station;

class ByDistanceTest {
	ByDistance test1 = new ByDistance("c", "a");
	ByDistance test2 = new ByDistance("c", "b");
	ByDistance test3 = new ByDistance("a", "c");
	ByDistance test4 = new ByDistance("a", "b");
	Station testa = new Station("a", "", "b", 0, 100);
	Station testb = new Station("b", "c", "a", 100, 100);
	Station testc = new Station("c", "b", "", 100, 0);
	List<Station> testline = new ArrayList<Station>();
	
	void getLine() {
		testline.add(testc);
		testline.add(testb);
		testline.add(testa);
	}
	
	@Test
	void testGetDistance() {
		getLine();
		assertEquals(200, test1.getDistance(testline), "Expected 200.");
		assertEquals(100, test2.getDistance(testline), "Expected 100.");
		assertEquals(200, test3.getDistance(testline), "Expected 200.");
		assertEquals(100, test4.getDistance(testline), "Expected 100.");
	}

	@Test
	void testCalculateFare() {
		getLine();
		double distance1 = test1.getDistance(testline);
		double distance2 = test2.getDistance(testline);
		double distance3 = test3.getDistance(testline);
		double distance4 = test4.getDistance(testline);
		assertEquals(41.1, test1.calculateFare(testline), "Expected 41.1");
		assertEquals(21.1, test2.calculateFare(testline), "Expected 21.1");
		assertEquals(41.1, test3.calculateFare(testline), "Expected 41.1");
		assertEquals(21.1, test4.calculateFare(testline), "Expected 21.1");
		assertEquals(test1.calculateFare(distance1), test1.calculateFare(testline), "Unexpected logical error");
		assertEquals(test2.calculateFare(distance2), test2.calculateFare(testline), "Unexpected logical error");
		assertEquals(test3.calculateFare(distance3), test3.calculateFare(testline), "Unexpected logical error");
		assertEquals(test4.calculateFare(distance4), test4.calculateFare(testline), "Unexpected logical error");
		assertEquals(41.1, test1.calculateFare(distance1), "Expected 41.1");
		assertEquals(21.1, test2.calculateFare(distance2), "Expected 21.1");
		assertEquals(41.1, test3.calculateFare(distance3), "Expected 41.1");
		assertEquals(21.1, test4.calculateFare(distance4), "Expected 21.1");
	}
}
