CREATE DATABASE afc;

\c afc;

CREATE SCHEMA afcdata;

CREATE TABLE afcdata.station
(
    name character varying(64) NOT NULL,
    nextstation character varying(64),
    distancetonext double precision DEFAULT 0,
    previousstation character varying(64),
    distancetoprevious double precision DEFAULT 0,
    PRIMARY KEY (name)
);

INSERT INTO afcdata.station (name, nextstation, distancetonext, previousstation, distancetoprevious)
VALUES
	('Perrache','Ampere - Victor Hugo',0.5,NULL,0),
	('Ampere - Victor Hugo','Bellecour',0.58,'Perrache',0.5),
	('Bellecour','Cordeliers',1.07,'Ampere - Victor Hugo',0.58),
	('Cordeliers','Hotel de Ville - Louis Pradel',0.39,'Bellecour',1.07),
	('Hotel de Ville - Louis Pradel','Foch',0.65,'Cordeliers',0.39),
	('Foch','Massena',0.69,'Hotel de Ville - Louis',0.65),
	('Massena','Charpennes - Charles Hernu',0.8,'Foch',0.69),
	('Charpennes - Charles Hernu','Republique - Villeurbanne',0.82,'Massena',0.8),
	('Republique - Villeurbanne','Gratte-Ciel',0.68,'Charpennes - Charles Hernu',0.82),
	('Gratte-Ciel','Flachet - Alain Gilles',0.6,'Republique - Villeurbanne',0.68),
	('Flachet - Alain Gilles','Cusset',0.9,'Gratte-Ciel',0.6),
	('Cusset','Laurent Bonnevay',0.65,'Flachet - Alain Gilles',0.9),
	('Laurent Bonnevay','Vaulx-en-Velin La Soie',1,'Cusset',0.65),
	('Vaulx-en-Velin La Soie',NULL,0,'Laurent Bonnevay',1);

CREATE TABLE afcdata.mapping
(
    barcode character varying(32) NOT NULL,
    id character(16) NOT NULL,
    CONSTRAINT mapping_pkey PRIMARY KEY (id)
);

INSERT INTO afcdata.mapping (barcode, id)
VALUES
	('abcasd','OW201912080000  '),
	('485asdf','OW201912100000  '),
	('sdw58234','OW201912110000  '),
	('saeq8874','OW201912110001  '),
	('789wer','OW201912110002  '),
	('455sdwe','OW201912150000  '),
	('4568wwe','OW201912160000  '),
	('rtew4we','OW201912180000  '),
	('stw408w','TF201912180000  '),
	('aeq6663we','TF201912180001  '),
	('sadqe245s','TF201912180002  '),
	('s45w2s8','TF201912180003  '),
	('aa45we1r','TF201912190000  '),
	('aeq8972s1e','TF201912190001  '),
	('sdq22','PC201911300000  '),
	('2849as4','PC201911300001  '),
	('48993se','PC201912050000  '),
	('5s6s8e','PC201912090000  '),
	('ty8w1r','PC201912110000  '),
	('sdq4102w5e','PC201912130000  '),
	('4eeqqdsa','TF201912260000  '),
	('ew35234','TF201912270000  '),
	('2311hfs3','TF201912270001  ');
	


CREATE TABLE afcdata.prepaidcard
(
    id character(16) NOT NULL,
    valid boolean NOT NULL,
    balance double precision,
    CONSTRAINT prepaidcard_pkey PRIMARY KEY (id),
    CONSTRAINT fk_id FOREIGN KEY (id)
        REFERENCES afcdata.mapping (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

INSERT INTO afcdata.prepaidcard (id, valid, balance)
VALUES
	('PC201911300000  ',False,12.2),
	('PC201911300001  ',False,0),
	('PC201912050000  ',True,0),
	('PC201912090000  ',True,15.3),
	('PC201912110000  ',True,7.3),
	('PC201912130000  ',True,3.75);

CREATE TABLE afcdata.onewayticket
(
    id character(16) NOT NULL,
    valid boolean NOT NULL,
    "from" character(64),
    "to" character(64),
    CONSTRAINT onewayticket_pkey PRIMARY KEY (id),
    CONSTRAINT fk_from FOREIGN KEY ("from")
        REFERENCES afcdata.station (name) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_id FOREIGN KEY (id)
        REFERENCES afcdata.mapping (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_to FOREIGN KEY ("to")
        REFERENCES afcdata.station (name) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

INSERT INTO afcdata.onewayticket (id, valid, "from", "to")
VALUES
	('OW201912080000  ',True,'Perrache','Gratte-Ciel'),
	('OW201912100000  ',False,'Foch','Cusset'),
	('OW201912110000  ',True,'Republique - Villeurbanne','Bellecour'),
	('OW201912110001  ',True,'Laurent Bonnevay','Cordeliers'),
	('OW201912110002  ',True,'Massena','Flachet - Alain Gilles'),
	('OW201912150000  ',False,'Charpennes - Charles Hernu','Perrache'),
	('OW201912160000  ',False,'Hotel de Ville - Louis Pradel','Vaulx-en-Velin La Soie'),
	('OW201912180000  ',True,'Ampere - Victor Hugo','Flachet - Alain Gilles');


CREATE TABLE afcdata.dayticket
(
    id character(16) NOT NULL,
    valid boolean NOT NULL,
    startdate bigint,
    balance double precision,
    CONSTRAINT daytoicket_pkey PRIMARY KEY (id),
    CONSTRAINT fk_id FOREIGN KEY (id)
        REFERENCES afcdata.mapping (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

INSERT INTO afcdata.dayticket (id, valid, startdate, balance)
VALUES
	('TF201912180000',False,1576658074,1.56),
	('TF201912180001',False,1576661640,0),
	('TF201912180002',True,1576671241,50),
	('TF201912180003',True,1576675361,15),
	('TF201912190000',True,1576736007,20),
	('TF201912190001',True,1576769067,4.2),
	('TF201912260000',True,1577359496,15),
	('TF201912270000',True,1577431843,20),
	('TF201912270001',True,1577463725,4.2);

CREATE TABLE afcdata.transaction
(
    id character(16) NOT NULL,
    "timestamp" bigint,
    start character varying(64) NOT NULL,
    stop character varying(64) NOT NULL,
    fare double precision,
    PRIMARY KEY ("timestamp"),
    CONSTRAINT fk_id FOREIGN KEY (id)
        REFERENCES afcdata.mapping (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_start FOREIGN KEY (start)
        REFERENCES afcdata.station (name) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_stop FOREIGN KEY (stop)
        REFERENCES afcdata.station (name) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);