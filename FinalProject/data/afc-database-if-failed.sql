CREATE DATABASE afc;

\c afc;

CREATE SCHEMA afcdata;

CREATE TABLE afcdata.station
(
    name character varying(64) NOT NULL,
    nextstation character varying(64),
    distancetonext double precision DEFAULT 0,
    previousstation character varying(64),
    distancetoprevious double precision DEFAULT 0,
    PRIMARY KEY (name)
);

CREATE TABLE afcdata.mapping
(
    barcode character varying(32) NOT NULL,
    id character(16) NOT NULL,
    CONSTRAINT mapping_pkey PRIMARY KEY (id)
);

CREATE TABLE afcdata.prepaidcard
(
    id character(16) NOT NULL,
    valid boolean NOT NULL,
    balance double precision,
    CONSTRAINT prepaidcard_pkey PRIMARY KEY (id),
    CONSTRAINT fk_id FOREIGN KEY (id)
        REFERENCES afcdata.mapping (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE afcdata.onewayticket
(
    id character(16) NOT NULL,
    valid boolean NOT NULL,
    "from" character(64),
    "to" character(64),
    CONSTRAINT onewayticket_pkey PRIMARY KEY (id),
    CONSTRAINT fk_from FOREIGN KEY ("from")
        REFERENCES afcdata.station (name) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_id FOREIGN KEY (id)
        REFERENCES afcdata.mapping (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_to FOREIGN KEY ("to")
        REFERENCES afcdata.station (name) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);


CREATE TABLE afcdata.dayticket
(
    id character(16) NOT NULL,
    valid boolean NOT NULL,
    startdate bigint,
    balance double precision,
    CONSTRAINT daytoicket_pkey PRIMARY KEY (id),
    CONSTRAINT fk_id FOREIGN KEY (id)
        REFERENCES afcdata.mapping (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE afcdata.transaction
(
    id character(16) NOT NULL,
    "timestamp" bigint,
    start character varying(64) NOT NULL,
    stop character varying(64) NOT NULL,
    fare double precision,
    PRIMARY KEY ("timestamp"),
    CONSTRAINT fk_id FOREIGN KEY (id)
        REFERENCES afcdata.mapping (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_start FOREIGN KEY (start)
        REFERENCES afcdata.station (name) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_stop FOREIGN KEY (stop)
        REFERENCES afcdata.station (name) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);