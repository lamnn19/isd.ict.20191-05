This is the repository for ISD.ICT.20191.
==============
Subject: ITSS Software Development
--------------
###### This is the instruction on how to get the database

*Group: 5*

*Nguyen Ngoc Lam*

*Dinh Phuong Nam*

1.	Database in used: ~~mysql~~ **postgresql**

	-	See documentation on [PostgreSQL 11](https://www.postgresql.org/docs/11/index.html)

	-	Download at [PostgreSQL Download](https://www.postgresql.org/download/); choose your os and follow the instruction; remember to get both psql (CLI) and pgadmin (GUI) for managing and developing your databases


2.	How to get the data:
	
	-	Step 1: locate the FinalProject/data folder from the repository.

	-	Step 2: open your CLI of choice (cmd on Windows, xterm or terminal on linux, terminal on mac OS)

	-	Step 3: run the following command: ***psql -l "path/to/FinalProject/data/afc-database.sql" -U postgres***

	-	Step 4: after step 3 you should get the database up and running with every tables under database named afc, schema afcdata. If not, *run the following command:* ***psql -l "path/to/FinalProject/data/afc-database-if-failed.sql" -U postgres***. It will get you the bare structure of database under the same database name and schema. *After that*, open pgadmin and ***import each csv file*** on FinalProject/data ***to the corresponding table***. (Noted that transaction table will be left empty)