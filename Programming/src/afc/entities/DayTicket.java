/**
 * Package contains representation of database data. In other word, each 
 * entities of of each class can be seen as a single row in one of the 
 * table in database
 */
package afc.entities;

/**
 * Actual implement of 24-hour
 * 
 * @version 1.1
 * @author nam
 *
 */
public class DayTicket extends Ticket {
	private long startdate = 0L;
	private double balance;

	/**
	 * Constructor of {@link DayTicket}
	 * 
	 * @param id      id of the ticket
	 * @param valid   last status of the ticket on database
	 * @param balance last balance status on database
	 */
	public DayTicket(String id, boolean valid, double balance) {
		super(id, valid);
		this.balance = balance;
	}

	/**
	 * Constructor of {@link DayTicket}
	 * 
	 * @param id         id of the ticket
	 * @param valid      last status of the ticket on database
	 * @param sTART_DATE a long number represented timestamp to second
	 * @param balance    last balance status on database
	 */
	public DayTicket(String id, boolean valid, long sTART_DATE, double balance) {
		super(id, valid);
		this.startdate = sTART_DATE;
		this.balance = balance;
	}

	/**
	 * Get the current balance of the ticket
	 * 
	 * @return current balance by euro as double
	 */
	public double getBalance() {
		return balance;
	}

	/**
	 * Get the timestamp the ticket had been initialized
	 * 
	 * @return timestamp as long (to second)
	 */
	public long getStartdate() {
		return startdate;
	}

	/**
	 * Used to compare two instances. At first version, use extensively. But after
	 * realizing if implement that way, system won't scale, change to one instance
	 * of ticket/card per 1 run (to push to database only).
	 * 
	 * @deprecated
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DayTicket other = (DayTicket) obj;
		if (Double.doubleToLongBits(balance) != Double.doubleToLongBits(other.balance))
			return false;
		if (startdate != other.startdate)
			return false;
		return true;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		if (super.valid) {
			return "DayTicket [id=" + super.getId() + ", start timestamp=" + startdate + ", balance=" + balance
					+ ", status:Approved" + "]";
		} else {
			return "DayTicket [id=" + super.getId() + ", start timestamp=" + startdate + ", balance=" + balance
					+ ", status:Denied" + "]";
		}
	}
}
