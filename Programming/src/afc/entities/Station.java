/**
 * Package contains representation of database data. In other word, each 
 * entities of of each class can be seen as a single row in one of the 
 * table in database
 */
package afc.entities;

/**
 * Each instance represent a single station in real life
 * 
 * @version 2.0
 * @author lam
 *
 */
public class Station {
	private String name;
	private String nextname;
	private String prevname;
	private double tonextstation = 0;
	private double toprevioustation = 0;

	/**
	 * Constructor of {@link Station}
	 * 
	 * @param name             name of the station
	 * @param nextname         name of the next station. null if terminal
	 * @param prevname         name of the previous station. null if terminal
	 * @param tonextstation    distance to next station (km). 0 if terminal
	 * @param toprevioustation distance to previous station (km). 0 if terminal
	 */
	public Station(String name, String nextname, String prevname, double tonextstation, double toprevioustation) {
		super();
		this.name = name;
		this.nextname = nextname;
		this.prevname = prevname;
		this.tonextstation = tonextstation;
		this.toprevioustation = toprevioustation;
	}

	/**
	 * Get the name of the station
	 * 
	 * @return station's name as a String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get the name of the next station
	 * 
	 * @return next station's name as the String
	 */
	public String getNextname() {
		return nextname;
	}

	/**
	 * Get the name of the previous station
	 * 
	 * @return previous station's name as the String
	 */
	public String getPrevname() {
		return prevname;
	}

	/**
	 * Get the distance from this station to the next next in line
	 * 
	 * @return distance to next station as double
	 */
	public double getTonextstation() {
		return tonextstation;
	}

	/**
	 * Get the distance from this station to the previous next in line
	 * 
	 * @return distance to previous station as double
	 */
	public double getToprevioustation() {
		return toprevioustation;
	}

	/**
	 * Hash function of station. Used to check. No longer needed.
	 * 
	 * @deprecated
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((nextname == null) ? 0 : nextname.hashCode());
		result = prime * result + ((prevname == null) ? 0 : prevname.hashCode());
		long temp;
		temp = Double.doubleToLongBits(tonextstation);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(toprevioustation);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/**
	 * Used to compare two instances. At first version, use extensively. But after
	 * realizing if implement that way, system won't scale, change to one instance
	 * of ticket/card per 1 run (to push to database only).
	 * 
	 * @deprecated
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Station other = (Station) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (nextname == null) {
			if (other.nextname != null)
				return false;
		} else if (!nextname.equals(other.nextname))
			return false;
		if (prevname == null) {
			if (other.prevname != null)
				return false;
		} else if (!prevname.equals(other.prevname))
			return false;
		if (Double.doubleToLongBits(tonextstation) != Double.doubleToLongBits(other.tonextstation))
			return false;
		if (Double.doubleToLongBits(toprevioustation) != Double.doubleToLongBits(other.toprevioustation))
			return false;
		return true;
	}

	/**
	 * Write out
	 */
	@Override
	public String toString() {
		if ((nextname == "") || (nextname == null)) {
			return "Terminal [name=" + name + ", previousstation=" + prevname + ", distancetoprevioustation="
					+ toprevioustation + "]";
		} else if ((prevname == "") || (prevname == null)) {
			return "Terminal [name=" + name + ", nextstation=" + nextname + ", distancetonextstation=" + tonextstation
					+ "]";
		} else {
			return "Station [name=" + name + ", previousstation=" + prevname + ", distancetoprevioustation="
					+ toprevioustation + ", nextstation=" + nextname + ", distancetonextstation=" + tonextstation + "]";
		}
	}
}
