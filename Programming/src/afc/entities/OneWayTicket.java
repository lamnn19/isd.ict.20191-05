/**
 * Package contains representation of database data. In other word, each 
 * entities of of each class can be seen as a single row in one of the 
 * table in database
 */
package afc.entities;

/**
 * Actual implement of one-way
 * 
 * @version 1.1
 * @author nam
 *
 */
public class OneWayTicket extends Ticket {
	private Station start;
	private Station destination;

	/**
	 * Constructor of {@link OneWayTicket}
	 * 
	 * @param id          id of the ticket
	 * @param valid       last status of the ticket on database
	 * @param start       start station (object)
	 * @param destination stop station (object)
	 */
	public OneWayTicket(String id, boolean valid, Station start, Station destination) {
		super(id, valid);
		this.start = start;
		this.destination = destination;
	}

	/**
	 * Get the start station
	 * 
	 * @return start station as an object
	 */
	public Station getStart() {
		return start;
	}

	/**
	 * Get the stop station
	 * 
	 * @return destination station as an object
	 */
	public Station getDestination() {
		return destination;
	}

	/**
	 * Used to compare two instances. At first version, use extensively. But after
	 * realizing if implement that way, system won't scale, change to one instance
	 * of ticket/card per 1 run (to push to database only).
	 * 
	 * @deprecated
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OneWayTicket other = (OneWayTicket) obj;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (start == null) {
			if (other.start != null)
				return false;
		} else if (!start.equals(other.start))
			return false;
		return true;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		if (super.valid) {
			return "OneWayTicket [id=" + super.getId() + ", start=" + start + ", destination=" + destination
					+ ", status:Approved" + "]";
		} else {
			return "OneWayTicket [id=" + super.getId() + ", start=" + start + ", destination=" + destination
					+ ", status:Denied" + "]";
		}
	}
}
