/**
 * Package contains representation of database data. In other word, each 
 * entities of of each class can be seen as a single row in one of the 
 * table in database
 */
package afc.entities;

import java.util.Date;

/**
 * Standardize information of one transaction. Used to push information to
 * database
 * 
 * @version 1.0
 * @author lam
 *
 */
public class Transaction {
	/**
	 * Ticket Instance
	 */
	private Ticket ticket = null;
	/**
	 * Card Instance
	 */
	private Card card = null;
	/**
	 * Timestamp of the transaction (in second)
	 */
	private long timestamp;
	/**
	 * Fare of the trip (even for one way ticket as they have start and stop
	 * station)
	 */
	private double fare;
	/**
	 * Station of which user start the journey
	 */
	private Station start;
	/**
	 * Station of which user stop the journey
	 */
	private Station stop;

	/**
	 * Constructor of {@link Transaction} with timestamp = now
	 * 
	 * @param ticket The ticket (object) of this transaction
	 * @param card   The card (object) of this transaction
	 * @param fare   Fare of the trip
	 * @param start  Station of which user start the journey
	 * @param stop   Station of which user stop the journey
	 */
	public Transaction(Ticket ticket, Card card, double fare, Station start, Station stop) {
		super();
		this.ticket = ticket;
		this.card = card;
		this.fare = fare;
		Date date = new Date();
		this.timestamp = date.getTime() / 1000;
		this.start = start;
		this.stop = stop;
	}

	/**
	 * Constructor of {@link Transaction} with timestamp = input param
	 * 
	 * @param ticket    The ticket (object) of this transaction
	 * @param card      The card (object) of this transaction
	 * @param timestamp Timestamp by user input
	 * @param fare      Fare of the trip
	 * @param start     Station of which user start the journey
	 * @param stop      Station of which user stop the journey
	 */
	public Transaction(Ticket ticket, Card card, long timestamp, double fare, Station start, Station stop) {
		super();
		this.ticket = ticket;
		this.card = card;
		this.timestamp = timestamp;
		this.fare = fare;
		this.start = start;
		this.stop = stop;
	}

	/**
	 * Get the ticket (on this problem either 24-hour or one way)
	 * 
	 * @return the ticket as object
	 */
	public Ticket getTicket() {
		return ticket;
	}

	/**
	 * Get the card (on this problem pre paid)
	 * 
	 * @return the card as object
	 */
	public Card getCard() {
		return card;
	}

	/**
	 * Get the timestamp (s)
	 * 
	 * @return the timestamp as long
	 */
	public long getTimestamp() {
		return timestamp;
	}

	/**
	 * Get the fare of this trip
	 * 
	 * @return the fare as double
	 */
	public double getFare() {
		return fare;
	}

	/**
	 * Get the start of the journey
	 * 
	 * @return the start as object
	 */
	public Station getStart() {
		return start;
	}

	/**
	 * Get the stop of the journey
	 * 
	 * @return the stop as object
	 */
	public Station getStop() {
		return stop;
	}

	/*
	 * Write out
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Transaction [" + (ticket != null ? "ticket=" + ticket + ", " : "")
				+ (card != null ? "card=" + card + ", " : "") + "timestamp=" + timestamp + ", fare=" + fare + "]";
	}

}
