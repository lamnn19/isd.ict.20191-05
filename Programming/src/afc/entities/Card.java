/**
 * Package contains representation of database data. In other word, each 
 * entities of of each class can be seen as a single row in one of the 
 * table in database
 */
package afc.entities;

/**
 * Super class of all card used in the system. Actual implement prepaid card
 * 
 * @version 1.1
 * @author lam
 *
 */
public class Card {
	private String id;
	public boolean valid;
	private double balance;

	/**
	 * Constructor of {@link Card}
	 * 
	 * @param id      id of the card
	 * @param valid   last status of the ticket on database
	 * @param balance last balance on the card on database
	 */
	public Card(String id, boolean valid, double balance) {
		super();
		this.id = id;
		this.valid = valid;
		this.balance = balance;
	}

	/**
	 * Get the id of the card
	 * 
	 * @return id as String
	 */
	public String getId() {
		return id;
	}

	/**
	 * Can the ticket open the gate or not
	 * 
	 * @return true if can open, false otherwise
	 */
	public boolean isValid() {
		return valid;
	}

	/**
	 * Get the number of remaining money on the card
	 * 
	 * @return money (euro) as a double
	 */
	public double getBalance() {
		return balance;
	}

	/**
	 * Used to compare two instances. At first version, use extensively. But after
	 * realizing if implement that way, system won't scale, change to one instance
	 * of ticket/card per 1 run (to push to database only).
	 * 
	 * @deprecated
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (balance != other.balance)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (valid != other.valid)
			return false;
		return true;
	}

	/**
	 * Write out
	 */
	@Override
	public String toString() {
		return "Card [id=" + id + ", valid=" + valid + ", balance=" + balance + "]";
	}
}
