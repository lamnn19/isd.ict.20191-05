package afc.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Utility class for connect and disconnect database
 * 
 * @version 2.0
 * @author lam
 * 
 */
public class DatabaseConnector {
	/**
	 * URL to database
	 */
	final String connection = "jdbc:postgresql://localhost/afc";
	/**
	 * user owned database. Default: postgres
	 */
	final String user = "postgres";
	/**
	 * password of the user.
	 */
	final String password = "admin";

	/**
	 * Connect to database using field above
	 * 
	 * @return conn {@link Connection} object
	 */
	public Connection connect() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(connection, user, password);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return conn;
	}

	/**
	 * Close existing connection
	 * 
	 * @param conn {@link Connection} object
	 * 
	 */
	public void close(Connection conn) {
		try {
			conn.close();
		} catch (Exception e) {
		}
	}

	/**
	 * Close existing connection, statement, resultset
	 * 
	 * @param conn {@link Connection} object
	 * @param stmt {@link Statement} object
	 * @param rs   {@link ResultSet} object
	 */
	public void close(Connection conn, Statement stmt, ResultSet rs) {
		try {
			rs.close();
			stmt.close();
			conn.close();
		} catch (Exception e) {
		}
	}

	/**
	 * Close existing connection, preparedstatement
	 * 
	 * @param conn  {@link Connection} object
	 * @param pstmt {@link PreparedStatement} object
	 */
	public void close(Connection conn, PreparedStatement pstmt) {
		try {
			pstmt.close();
			conn.close();
		} catch (Exception e) {
		}
	}
}
