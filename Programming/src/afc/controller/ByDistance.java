package afc.controller;

import java.util.ArrayList;
import java.util.List;

import afc.entities.Station;

/**
 * Calculate fare by distance between start and stop station. Not relevance if
 * user has one-way ticket
 * 
 * @version 1.0
 * @author nam
 *
 */
public class ByDistance extends FareCalculator {

	/**
	 * Constructor of {@link ByDistance}. Follow construction of super class.
	 * 
	 * @param start name of start station
	 * @param stop  name of stop station
	 */
	public ByDistance(String start, String stop) {
		super(start, stop);
	}

	/**
	 * Method to find start and stop on line and get the total distance between
	 * them.
	 * 
	 * @param line list of station (in 1 line)
	 * @return total distance (sum of to next or to previous) (km)
	 */
	public double getDistance(List<Station> line) {
		List<String> passed = new ArrayList<String>();
		double distance = 0;
		for (int i = 0; i < line.size(); i++) {
			if (line.get(i).getName() == super.getStart()) {
				if (!passed.contains(super.getStop())) {
					for (int j = i; j < line.size(); j++) {
						distance += line.get(j).getTonextstation();
						if (line.get(i).getNextname().equals(super.getStop()))
							return distance;
					}
					break;
				} else {
					int stopindex = passed.indexOf(super.getStop());
					for (int k = stopindex; k < i; k++) {
						distance += line.get(k).getTonextstation();
					}
					return distance;
				}
			} else {
				passed.add(line.get(i).getName());
			}
		}
		return distance;
	}

	/**
	 * Calculate fare by distance between station on line
	 */
	@Override
	public double calculateFare(List<Station> line) {
		double fare = 1.9;
		double distance = this.getDistance(line);
		System.out.println(distance);
		if (distance < 5)
			return fare;
		else {
			if ((int) ((distance - 5) / 2) < ((distance - 5) / 2))
				return fare + ((int) ((distance - 5) / 2) + 1) * 0.4;
			else
				return fare + ((int) ((distance - 5) / 2)) * 0.4;
		}
	}

	/**
	 * Calculate fare of two points given distance of them
	 * 
	 * @param distance how far 2 points to each other
	 * @return fare counted by euro
	 * @deprecated
	 */
	public double calculateFare(double distance) {
		double fare = 1.9;
		if (distance < 5)
			return fare;
		else {
			if ((int) ((distance - 5) / 2) < ((distance - 5) / 2))
				return fare + ((int) ((distance - 5) / 2) + 1) * 0.4;
			else
				return fare + ((int) ((distance - 5) / 2)) * 0.4;
		}
	}
}
