package afc.controller;

import java.util.List;

import afc.entities.Station;

/**
 * Super class of all method of calculating fare. Only implement by distance
 * method. Originally wanted to implement by command design pattern but cannot
 * fix in time.
 * 
 * @version 1.0
 * @author nam
 *
 */
public class FareCalculator {
	private String start;
	private String stop;

	/**
	 * Constructor of {@link FareCalculator}
	 * 
	 * @param start name of start station
	 * @param stop  name of stop station
	 */
	public FareCalculator(String start, String stop) {
		super();
		this.start = start;
		this.stop = stop;
	}

	/**
	 * Get the start station name
	 * 
	 * @return name of the start station as String
	 */
	public String getStart() {
		return start;
	}

	/**
	 * Get the stop station name
	 * 
	 * @return name of the stop station as String
	 */
	public String getStop() {
		return stop;
	}

	/**
	 * Base method of calculate fare
	 * 
	 * @param line show the train run on which line
	 * @return fare counted by euro
	 */
	public double calculateFare(List<Station> line) {
		return 0;
	}
}
