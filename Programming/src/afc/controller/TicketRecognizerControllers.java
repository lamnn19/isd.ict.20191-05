package afc.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import afc.entities.DayTicket;
import afc.entities.OneWayTicket;
import afc.entities.PrepaidCard;
import afc.entities.Station;
import afc.entities.Ticket;

/**
 * Controller class of ticket recognizer. Included: change from pseudo bar code
 * to ticket id and get Ticket base on type and id
 * 
 * @version 2.0
 * @author lam
 *
 */
public class TicketRecognizerControllers {
	/**
	 * Change from pseudo bar code to ticket id
	 * 
	 * @param barcode bar code that the user has
	 * @return ticket id (started with either OW or TF); null otherwise (included
	 *         invalid bar code or bar code of a card)
	 */
	public String getTicketId(String barcode) {
		DatabaseConnector ct = new DatabaseConnector();
		Connection conn = ct.connect();
		String param = "'" + barcode + "'";
		String result = null;

		String SQL = "select id from afcdata.mapping as m where m.barcode = " + param;
		try (Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(SQL)) {
			while (rs.next()) {
				result = rs.getString("id");
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		String pattern1 = "^OW";
		String pattern2 = "^TF";
		Pattern r1 = Pattern.compile(pattern1);
		Pattern r2 = Pattern.compile(pattern2);
		Matcher m1 = r1.matcher(result);
		Matcher m2 = r2.matcher(result);
		if ((!m1.find()) && (!m2.find()))
			result = null;

		ct.close(conn);
		return result;
	}

	/**
	 * Get a station.
	 * 
	 * @param name Name of that station
	 * @return one Station with name.
	 */
	public Station getStation(String name) {
		DatabaseConnector ct = new DatabaseConnector();
		Connection conn = ct.connect();
		String param = "'" + name + "'";

		String SQL = "select * from afcdata.station where name = " + param;
		try (Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(SQL)) {
			while (rs.next()) {
				String nname = rs.getString("name");
				String next = rs.getString("nextstation");
				double dn = rs.getDouble("distancetonext");
				String previous = rs.getString("previousstation");
				double dp = rs.getDouble("distancetoprevious");
				Station toappend = new Station(nname, next, previous, dn, dp);
				return toappend;
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		ct.close(conn);
		return null;
	}

	/**
	 * Get instance of one-way ticket
	 * 
	 * @param id id of the instance
	 * @return an {@link OneWayTicket} object if id started with OW; null otherwise
	 */
	public OneWayTicket getOneWayTicket(String id) {
		String pattern = "^OW";
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(id);
		DatabaseConnector ct = new DatabaseConnector();
		Connection conn = ct.connect();
		if (m.find()) {
			String param = "'" + id + "'";
			String SQL = "select * from afcdata.onewayticket where id = " + param;
			try (Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(SQL)) {
				while (rs.next()) {
					String nid = rs.getString("id");
					boolean nvalid = rs.getBoolean("valid");
					String nfrom = rs.getString("from");
					String nto = rs.getString("to");
					Station from = getStation(nfrom);
					Station to = getStation(nto);
					OneWayTicket result = new OneWayTicket(nid, nvalid, from, to);
					return result;
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
		}
		ct.close(conn);
		return null;
	}

	/**
	 * Get instance of 24-hour ticket
	 * 
	 * @param id id of the instance
	 * @return an {@link DayTicket} object if id started with TF; null otherwise
	 */
	public DayTicket getDayTicket(String id) {
		String pattern = "^TF";
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(id);
		DatabaseConnector ct = new DatabaseConnector();
		Connection conn = ct.connect();
		if (m.find()) {
			String param = "'" + id + "'";
			String SQL = "select * from afcdata.dayticket where id = " + param;
			try (Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(SQL)) {
				while (rs.next()) {
					String nid = rs.getString("id");
					boolean nvalid = rs.getBoolean("valid");
					Long nstamp = (long)rs.getInt("startdate");
					long ndate = (long) nstamp;
					double nbalance = rs.getDouble("balance");
					DayTicket result = new DayTicket(nid, nvalid, ndate, nbalance);
					ct.close(conn);
					return result;
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
		}
		ct.close(conn);
		return null;
	}
}
