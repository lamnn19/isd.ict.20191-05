package afc.controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import afc.entities.PrepaidCard;

class CardScannerControllersTest {
	CardScannerControllers test = new CardScannerControllers();

	@Test
	void testGetCardId() {
		assertEquals(null, test.getCardId("abcasd"), "Unexpected error");
		assertEquals("PC201912090000  ", test.getCardId("5s6s8e"), "Unexpected error");
	}

	@Test
	void testGetPrepaidCard() {
		PrepaidCard testcard = new PrepaidCard("PC201912090000  ", true, 15.3);
		assertEquals(testcard, test.getPrepaidCard("PC201912090000  "), "Unexpected error");
		assertEquals(null, test.getPrepaidCard("dC201912090000  "), "Unexpected error");
	}
}
