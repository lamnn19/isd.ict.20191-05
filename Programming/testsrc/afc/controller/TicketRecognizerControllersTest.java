package afc.controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import afc.entities.DayTicket;
import afc.entities.OneWayTicket;
import afc.entities.Station;

class TicketRecognizerControllersTest {
	TicketRecognizerControllers test = new TicketRecognizerControllers();

	@Test
	void testGetTicketId() {
		assertEquals("OW201912080000  ", test.getTicketId("abcasd"), "Unexpected error");
		assertEquals("TF201912190001  ", test.getTicketId("aeq8972s1e"), "Unexpected error");
		assertEquals(null, test.getTicketId("5s6s8e"), "Unexpected error");
	}

	@Test
	void testGetOneWayTicket() {
		Station start = test.getStation("Perrache");
		Station stop = test.getStation("Gratte-Ciel");
		OneWayTicket testticket = new OneWayTicket("OW201912080000  ", true, start, stop);
		assertEquals("Perrache                                                        ", start.getName());
		assertEquals(testticket, test.getOneWayTicket("OW201912080000  "), "Logical error");
		assertEquals(null, test.getOneWayTicket("TF201912190001  "), "Unexpected error");
	}

	@Test
	void testGetDayTicket() {
		DayTicket testticket = new DayTicket("TF201912190001                                                  ", true,
				1576769067L * 1000, 4.2);
		assertEquals(testticket, test.getDayTicket("TF201912190001  "), "Logical error");
		assertEquals(null, test.getDayTicket("OW201912080000  "), "Unexpected error");
	}
}
