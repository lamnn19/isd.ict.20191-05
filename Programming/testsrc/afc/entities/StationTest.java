package afc.entities;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class StationTest {
	Station testa = new Station("a", "", "b", 0, 100);
	Station testb = new Station("b", "c", "a", 100, 100);
	Station testc = new Station("c", "b", "", 100, 0);
	Station test = new Station("d", "", "", 0, 0);
	
	@Test
	void testEqualsObject() {
		Station a = new Station("a", "", "b", 0, 100);
		Station b = new Station("b", "", "b", 0, 100);
		Station c = new Station("a", "b", "b", 0, 100);
		Station d = new Station("a", "", "", 0, 100);
		Station e = new Station("a", "", "b", 1, 100);
		Station f = new Station("a", "", "b", 0, 1000);
		assertEquals(true, testa.equals(a), "Unexpected false return.");
		assertEquals(false, testa.equals(b), "Do not implement on name field");
		assertEquals(false, testa.equals(c), "Do not implement on nextname field");
		assertEquals(false, testa.equals(d), "Do not implement on prevname field");
		assertEquals(false, testa.equals(e), "Do not implement on tonextstation field");
		assertEquals(false, testa.equals(f), "Do not implement on topreviousstation field");
	}
	
	@Test
	void testGetName() {
		assertEquals("a", testa.getName(), "Unexpected error.");
		assertEquals("b", testb.getName(), "Unexpected error.");
		assertEquals("c", testc.getName(), "Unexpected error.");
		assertEquals("d", test.getName(), "Unexpected error.");
	}

	@Test
	void testGetNextname() {
		assertEquals("", testa.getNextname(), "Unexpected error.");
		assertEquals("c", testb.getNextname(), "Unexpected error.");
		assertEquals("b", testc.getNextname(), "Unexpected error.");
		assertEquals("", test.getNextname(), "Unexpected error.");
	}

	@Test
	void testGetPrevname() {
		assertEquals("b", testa.getPrevname(), "Unexpected error.");
		assertEquals("a", testb.getPrevname(), "Unexpected error.");
		assertEquals("", testc.getPrevname(), "Unexpected error.");
		assertEquals("", test.getPrevname(), "Unexpected error.");
	}

	@Test
	void testGetTonextstation() {
		assertEquals(0, testa.getTonextstation(), "Unexpected error.");
		assertEquals(100, testb.getTonextstation(), "Unexpected error.");
		assertEquals(100, testc.getTonextstation(), "Unexpected error.");
		assertEquals(0, test.getTonextstation(), "Unexpected error.");
	}

	@Test
	void testGetToprevioustation() {
		assertEquals(100, testa.getToprevioustation(), "Unexpected error.");
		assertEquals(100, testb.getToprevioustation(), "Unexpected error.");
		assertEquals(0, testc.getToprevioustation(), "Unexpected error.");
		assertEquals(0, test.getToprevioustation(), "Unexpected error.");
	}


	@Test
	void testToString() {
		String expected1 = "Terminal [name=" + testa.getName() + ", previousstation=" + testa.getPrevname() 
				+ ", distancetoprevioustation=" + testa.getToprevioustation() + "]";
		String expected2 = "Terminal [name=" + testc.getName() + ", nextstation=" + testc.getNextname() 
				+ ", distancetonextstation=" + testc.getTonextstation() + "]";
		String expected3 = "Station [name=" + testb.getName() + ", previousstation=" + testb.getPrevname() 
				+ ", distancetoprevioustation=" + testb.getToprevioustation()  + ", nextstation=" 
				+ testb.getNextname() + ", distancetonextstation=" + testb.getTonextstation() + "]";
		assertEquals(expected1, testa.toString(), "Error in format output");
		assertEquals(expected2, testc.toString(), "Error in format output");
		assertEquals(expected3, testb.toString(), "Error in format output");
	}
}
