package afc.entities;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.Test;

class DayTicketTest {
	DayTicket ticket = new DayTicket("i", true, 0, 10000);
	DayTicket ticket1 = new DayTicket("iesda3", true, 0, 10000);
	DayTicket ticket2 = new DayTicket("sdadsi", false, 1574328753L, 0);
	
	@Test
	void testEqualsObject() {
		DayTicket test = new DayTicket("i", true, 0, 10000);
		DayTicket test3 = new DayTicket("i", true, 1574328753L, 10000);
		DayTicket test4 = new DayTicket("i", true, 0, 0);
		assertEquals(true, ticket.equals(test), "Unexpected false return");
		assertEquals(false, ticket.equals(ticket1), "Do not inplement on id field");
		assertEquals(false, ticket.equals(ticket2), "Do not inplement on valid field");
		assertEquals(false, ticket.equals(test3), "Do not inplement on startdate field");
		assertEquals(false, ticket.equals(test4), "Do not inplement on balance field");
	}
	
	@Test
	void testGetBalance() {
		assertEquals(10000, ticket.getBalance(), "Expected 10000. Get " + ticket.getBalance() + " instead.");
		assertEquals(10000, ticket1.getBalance(), "Expected 10000. Get " + ticket1.getBalance() + " instead.");
		assertEquals(0, ticket2.getBalance(), "Expected 0. Get " + ticket2.getBalance() + " instead.");
	}

	@Test
	void testGetId() {
		assertEquals("i", ticket.getId(), "Expected value of i. Get " + ticket.getId() + " instead.");
		assertEquals("iesda3", ticket1.getId(), "Expected value of iesda3. Get " + ticket1.getId() + " instead.");
		assertEquals("sdadsi", ticket2.getId(), "Expected value of sdadsi. Get " + ticket2.getId() + " instead.");
	}

	@Test
	void testGetStartdate() {
		assertEquals(0, ticket.getStartdate(), "Unexpected error.");
		assertEquals(1574328753, ticket2.getStartdate(), "Unexpected error.");
	}

	@Test
	void testToString() {
		String expected1 = "DayTicket [id=" + ticket.getId()  + ", start timestamp=" + ticket.getStartdate() 
							+ ", balance=" + ticket.getBalance() + ", status:Approved" + "]";
		String expected2 = "DayTicket [id=" + ticket2.getId()  + ", start timestamp=" + ticket2.getStartdate() 
							+ ", balance=" + ticket2.getBalance() + ", status:Denied" + "]";
		assertEquals(expected1, ticket.toString(), "Unexpected errors.");
		assertEquals(expected2, ticket2.toString(), "Unexpected errors.");
	}
}
